﻿using AnswerKing.WebAPI.Models;
using AnswerKing.WebAPI.Repository;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace AnswerKing.WebAPI.Controllers
{
    [Route("api/{controller}")] 
    [ApiController]
    public class ItemsController : ControllerBase
    {
        private IItemRepository ItemRepository { get; }

        public ItemsController(IItemRepository itemRepository) => this.ItemRepository = itemRepository;
        
        // GET:     api/items
        [HttpGet]
        public ActionResult<IEnumerable<Item>> GetAllItems()
        {
            var items = this.ItemRepository.GetAllItems();
            return Ok(items);
        }

        // GET:     api/items/{id}
        [HttpGet("{id}")]
        public ActionResult<Item> GetItemById(int id)
        {
            var item = this.ItemRepository.GetItemById(id);
            return Ok(item);
        }
    }
}
