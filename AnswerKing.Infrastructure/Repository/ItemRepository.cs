﻿using AnswerKing.WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AnswerKing.WebAPI.Repository
{
    public class ItemRepository : IItemRepository
    {
        private IList<Item> ItemCollection { get; set; }

        public ItemRepository() => this.ItemCollection = InMemoryCollection;

        public IEnumerable<Item> GetAllItems()
        {
            return this.ItemCollection;
        }

        public Item GetItemById(int id)
        {
            var item = this.ItemCollection.SingleOrDefault(i => i.Id == id);

            if (item == null)
            {
                throw new ArgumentNullException();
            }

            return item;
        }

        private static IList<Item> InMemoryCollection { get; } = new List<Item>
        {
            new Item() { Id = 1, Name ="Whopper", CurrentPrice = 3.99m, Description ="Answer King's Whopper Burger"},
            new Item() { Id = 2, Name = "Angus", CurrentPrice = 4.99m, Description = "Answer King's Angus Burger" },
            new Item() { Id = 3, Name = "Double Patty", CurrentPrice = 4.50m, Description = "Answer King's Whopper Burger with a double patty" },
            new Item() { Id = 4, Name = "Answer King", CurrentPrice = 4.99m, Description = "The Signature Burger from Answer King" },
            new Item() { Id = 5, Name = "Kids Cheeseburger", CurrentPrice = 2.99m, Description = "Answer King loves your kids!" },
        };

        /*
        public async Task<IActionResult> Create(Item item)
        {

        }
        */
    }
}
