﻿using AnswerKing.WebAPI.Models;
using System.Collections.Generic;

namespace AnswerKing.WebAPI.Repository
{
    public interface IItemRepository
    {
        IEnumerable<Item> GetAllItems();
        Item GetItemById(int id);
    }
}
